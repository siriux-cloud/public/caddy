# caddy

Imagen Publica de Caddy

# Variables

- CADDYFILE_TEMPLATE: Indica un template de gotemplate para personalizar o construir el Caddyfile final.
- CADDY_CLUSTERING_REDIS_ENABLED: Activa el soporte de clustes con redis como cache de peticiones!


# Activar Cluster.

Para activar cluster con Redis, se debe proveer un Caddyfile que comienze con

```
storage redis
```

No se puede estabvlecer de forma dinamica en la imagen ya que el mecanismo de interpolacion natural del Caddyfile `{$VARIABLE}` no tiene ninguna mecanismo de toma de decisiones.


Sin embargo es posible establecer un template para Caddyfile con la variable de entorno `CADDYFILE_TEMPLATE`, que ejecutara un template para establecer el Caddyfile.

Ejemplo.

```go
{{- /* Go Template file */ -}}

{{ if eq (getenv "CADDY_CLUSTERING_REDIS_ENABLED" "false") "true" }}
{
	storage redis
}
{{ end }}

#  A continuacion establesca las reglas de su Caddyfile, Puede usar la sintaxis de gomplate si las reglas son dinamicas o simplemente usar el mecanismo de Caddyfile.

:80 {
	# Set this path to your site's directory.
	root * /usr/share/caddy

	# Enable the static file server.
	file_server

	# Another common task is to set up a reverse proxy:
	# reverse_proxy localhost:8080

	# Or serve a PHP site through php-fpm:
	# php_fastcgi localhost:9000
}
```