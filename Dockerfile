ARG CADDY_VERSION=2.7.6

FROM caddy:${CADDY_VERSION}-builder-alpine AS builder

RUN xcaddy build \
    --with github.com/caddy-dns/digitalocean \
    --with github.com/gamalan/caddy-tlsredis

FROM caddy:${CADDY_VERSION}-alpine

RUN apk add --no-cache jq wget

# Instalamos gomplate para manipular templates
ARG GOMPLATE_VERSION="3.11.5"
RUN wget https://github.com/hairyhenderson/gomplate/releases/download/v${GOMPLATE_VERSION}/gomplate_linux-amd64 -O /usr/bin/gomplate && \
    chmod +x /usr/bin/gomplate

ARG YQ_VERSION="4.35.2"
RUN wget https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_linux_amd64 -O /usr/bin/yq && \
    chmod +x /usr/bin/yq
    
# Copiamos el binario de caddy desde el builder
COPY --from=builder /usr/bin/caddy /usr/bin/caddy

# Copiamos el contenido de la carpeta docker-context a la raiz del contenedor
COPY /root/ /

RUN chmod +x /overryde-entrypoint.sh

RUN cat /overryde-entrypoint.sh

ENTRYPOINT ["/overryde-entrypoint.sh"]

CMD ["caddy", "run", "--config", "/etc/caddy/Caddyfile", "--adapter", "caddyfile"]
