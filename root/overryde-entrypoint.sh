#!/bin/sh

set -Eeo pipefail

# Path del script
SCRIPT_PATH="$(dirname "$0")"                # relative
SCRIPT_PATH="$(cd "$SCRIPT_PATH" && pwd)" # absolutized and normalized
if [ -z "$SCRIPT_PATH" ]; then
    # error; for some reason, the path is not accessible
    # to the script (e.g. permissions re-evaled after suid)
    exit 1 # fail
fi

if [ -n "$CADDYFILE_TEMPLATE" ]; then
    echo "Se ha definido la variable de entorno CADDYFILE_TEMPLATE, se usara el Caddyfile personalizado."

    gomplate \
        --config "/etc/caddy/.gomplate.yaml" \
        -f "${CADDYFILE_TEMPLATE}" \
        -V \
        >"/etc/caddy/Caddyfile"

    # Formateamos el Caddyfile
    caddy fmt --overwrite  "/etc/caddy/Caddyfile"
fi

exec "$@"
